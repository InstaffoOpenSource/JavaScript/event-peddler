import test from 'p-tape'

import makeEventPeddler from '../ep.node'

test('basics', async(t) => {
  t.plan(4)
  const emit = makeEventPeddler()

  const nope = {
    bothered: false,
    onNoes(...args) { this.bothered = args },
  }

  const logAll = {
    log: [],
    onInfo(...args) { this.log.push(['i', ...args]) },
    onWarn(...args) { this.log.push(['w', ...args]) },
  }

  const logWarn = {
    log: [],
    onWarn(...args) { this.log.push(args) },
  }

  emit.sub(nope)
  emit.sub([logAll, logWarn]) // test whether we can add targets later

  const fchm = function funcsCanHaveMethods(...args) { fchm.yay = args }
  fchm.onInfo = fchm
  emit.sub([fchm])

  emit('Info', { prio: 51 })
  emit('Warn', 'regexp', /rexhep/)
  emit('Info', 'Das ist korrekt')
  emit('Warn', { kurzeFrage: '100% sicher?' })

  await new Promise(solve => setTimeout(solve, 50))
  t.equal(nope.bothered, false)
  t.deepEqual(logAll.log, [
    ['i', { prio: 51 }],
    ['w', 'regexp', /rexhep/],
    ['i', 'Das ist korrekt'],
    ['w', { kurzeFrage: '100% sicher?' }],
  ])
  t.deepEqual(logWarn.log, [
    ['regexp', /rexhep/],
    [{ kurzeFrage: '100% sicher?' }],
  ])
  t.deepEqual(fchm.yay, ['Das ist korrekt'])
})
