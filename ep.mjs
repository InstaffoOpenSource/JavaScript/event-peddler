function mightHaveMethods(x) {
  if (!x) { return false }
  const t = typeof x
  return ((t === 'object') || (t === 'function'))
}

function makeEventPeddler(customize) {
  const pe = function peddleEvent(evName, ...details) {
    const evKey = `on${evName}`
    const interested = pe.potentialTargets.filter(
      tgt => (typeof tgt[evKey] === 'function'))
    if (pe.onBeforePeddle) {
      pe.onBeforePeddle.call(pe, interested, evName, details)
    }
    interested.forEach(tgt => setImmediate(
      () => tgt[evKey].call(tgt, ...details)))
    return interested.length
  }
  pe.potentialTargets = []
  pe.nPot = () => pe.potentialTargets.length
  function meet(newPots) {
    if (!Array.isArray(newPots)) { return meet([newPots]) }
    pe.potentialTargets.push(...newPots.filter(mightHaveMethods))
    return pe
  }
  pe.subscribeThese = meet
  pe.sub = meet
  pe.meet = meet
  Object.assign(pe, customize)
  return pe
}

export default makeEventPeddler
