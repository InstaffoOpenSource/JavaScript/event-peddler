﻿
<!--#echo json="package.json" key="name" underline="=" -->
@instaffogmbh/event-peddler
===========================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
Light-weight notification mechanism with 3rd party subscription, like
unverified newsletters.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



API
---

This module exports one function:

### emit = makeEventPeddler()

Returns a function with a list of potential targets in its
`potentialTargets` property.
Call `emit(evName[, ...args])` to notify all targets that have a
property named `'on' + evName`.

Methods on `emit`:
* `nPot`: Returns the current number (length) of `potentialTargets`.
* `subscribeThese`: Expects an array as argument.
  Adds all objects in that array to `potentialTargets`.
  Aliases: `sub`, `meet`




<!--#toc stop="scan" -->


&nbsp;


License
-------
<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
